import logo from './logo.svg';
import './App.css';

 //remove the template in the module, to give way to the new line of code that we will create for the app.

 //when compiling multiple components, they should be wrapped inside a single element.

 //Save an image of your favorite cartoon character inside the public folder.  

 //NEW SKILL: Identify how to access resources and elements from the public folder/library of the project. 

 ///PUBLIC -> to store all documents and modules that shared accross all components of the app that are visible to all. 

 //to acquire the image that you want to display, you have to get it from the public folder. import the element inside this module
 //  the resource that was target was NOT saved in the pulic folder, so the file has to be imported for it to become usable.
import cartoon from './cartoon.png';
  //pass down the alias that identifies the resource into our element.

function App() {
  return (
    <div>
      <h1>This the course booking of batch 164</h1>
      <h5>This is our project in React</h5>
      <h6>Come Visit our website</h6> 
      <img src={cartoon} alt="image not found" />
      <img src="/image/cartoon.png" />
      <h3>This is My Favorite Cartoon Character</h3>  
    </div>
    );
}

export default App;
